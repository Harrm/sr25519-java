# Sr25519-Java

## Overview
This is an example project of binding a **Rust*** library *schnorrkel* to an Android **Java** application.
It uses **Rust FFI** (Foreign Function Interface) to achieve binary interface compatibility of 
a native lib (built using *schnorrkel* wrapped in some glue code) and the Java app, 
which contains a specifically defined class **com.soramitsu.crypto.Sr25519**. 
It contains **native** methods with signatures matching certain functions in the glue code that are defined to be exported.
Therefore, these **native** methods of **com.soramitsu.crypto.Sr25519** are entry points to 
the library *sr25519-java* (the one with the glue code) that wraps some functionality of *schnorrkel*.

## Build
A Mozilla plugin for Gradle that allows building Rust projects as part of Gradle tasks is used.
The usage literally follows the tutorial of the plugin, so the only thing to mention here is that
in the rules to build the rust project (defined in *build.gradle* of the android app),
```
cargo {
    module = "../sr25519-java/"
    libname = "sr25519java"
    targets = ["arm", "x86"]
    targetIncludes = ['libsr25519java.so']
}
```
The libname must match the library name defined in Rust's Cargo.toml under \[lib]...name = "sr25519java".

## Structure
* app/src/main/java/ - code of an example mobile application (com.example.myapplication) and
a class with native methods that is to be binded with Rust implementation (com.soramitsu.crypto.Sr25519)

* sr25519-java/ - the Rust glue code for binding to schnorrkel lib.

## Links
* Rust Android Gradle Plugin - https://github.com/mozilla/rust-android-gradle/
* Rust Foreign Function Interface - https://doc.rust-lang.org/nomicon/ffi.html
* Good article about Rust for Android - https://medium.com/visly/rust-on-android-19f34a2fb43